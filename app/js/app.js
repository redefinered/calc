var app = angular.module('mortgage-payment-calc', ['nvd3']);

app.controller('MainCtrl', function ($scope, $http) {

    $scope.loan_amount = 250000.00;
    $scope.interest_rate = 5.000;
    $scope.payments_per_year = 12;
    $scope.years = 30;
    $scope.interest_rate_per_month = ($scope.interest_rate/100)/12;

    var data = {
        calculator: 'BuyVsRentCalculator',
        template: 'SingleBuyVsRentCalculator',
        'calculatorData[calculator_widget][PeriodRent]':800.00,
        'calculatorData[calculator_widget][AnnualRentIncrease]':4.000,
        'calculatorData[calculator_widget][HomeValue]':300000.00,
        'calculatorData[calculator_widget][AnnualMaintanance]':900.00,
        'calculatorData[calculator_widget][AnnualAppreciation]':5.000,
        'calculatorData[calculator_widget][YearsBeforeSell]':5,
        'calculatorData[calculator_widget][SellingCost]':7.000,
        'calculatorData[calculator_widget][Amount]':250000.00,
        'calculatorData[calculator_widget][Interest]':5.000,
        'calculatorData[calculator_widget][Length]':30,
        'calculatorData[calculator_widget][Points]':1.000,
        'calculatorData[calculator_widget][Taxes]':26.000,
        'calculatorData[calculator_widget][PropertyTaxes]':3000.00,
        'calculatorData[calculator_widget][PropertyTaxesSel]':0,
        'calculatorData[calculator_widget][Insurance]':1500.00,
        'calculatorData[calculator_widget][InsuranceSel]':0,
        'calculatorData[calculator_widget][PMI]':0.500,
        'calculatorData[calculator_widget][PMISel]':1
    }

    $http({
        method: 'POST',
        url: 'http://localhost/calc/app/api.php',
        data: $.param(data),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(function(res){
        console.log(res);
        var elem = angular.element(res);
        console.log(elem.find('strong:contains("Total Payments")').parents('tr').find('td:nth-child(2)').text());
    }, function(err){
        console.log(err);
    });

    $scope.mpsdata = chart_mortgagePaymentSchedule();
    // $scope.rvbdata = chart_rentVsBuy();

    $scope.onInputChange = function() {
        $scope.mpsdata = chart_mortgagePaymentSchedule();
        // $scope.rvbdata = chart_rentVsBuy();
    }

    $scope.options_mortgagePaymentSchedule = {
        chart: {
            type: 'lineChart',
            height: 450,
            margin: {
                top: 20,
                right: 55,
                bottom: 40,
                left: 20
            },
            x: function (d) { return d.x; },
            y: function (d) { return d.y; },
            useInteractiveGuideline: true,
            dispatch: {
                stateChange: function (e) { console.log("stateChange"); },
                changeState: function (e) { console.log("changeState"); },
                tooltipShow: function (e) { console.log("tooltipShow"); },
                tooltipHide: function (e) { console.log("tooltipHide"); }
            },
            xAxis: {
                axisLabel: 'No. of Months'
            },
            yAxis: {
                axisLabel: 'Interest & Principal',
                tickFormat: function (d) {
                    return d3.format('.02f')(d);
                },
                axisLabelDistance: -10
            },
            callback: function (chart) {
                console.log("!!! lineChart callback !!!");
            }
        },
        title: {
            enable: true,
            text: 'Mortgage Payment Schedule'
        }
    };

    $scope.options_rentVsBuy = {
        chart: {
            type: 'lineChart',
            height: 450,
            margin: {
                top: 20,
                right: 55,
                bottom: 40,
                left: 20
            },
            x: function (d) { return d.x; },
            y: function (d) { return d.y; },
            useInteractiveGuideline: true,
            dispatch: {
                stateChange: function (e) { console.log("stateChange"); },
                changeState: function (e) { console.log("changeState"); },
                tooltipShow: function (e) { console.log("tooltipShow"); },
                tooltipHide: function (e) { console.log("tooltipHide"); }
            },
            xAxis: {
                axisLabel: 'No. of Months'
            },
            yAxis: {
                axisLabel: 'Interest & Principal',
                tickFormat: function (d) {
                    return d3.format('.02f')(d);
                },
                axisLabelDistance: -10
            },
            callback: function (chart) {
                console.log("!!! lineChart callback !!!");
            }
        },
        title: {
            enable: true,
            text: 'Mortgage Payment Schedule'
        }
    };


    // custommize chart
    $scope.options_mortgagePaymentSchedule.chart.rightAlignYAxis = true;

    // calculate payment
    function pmt(rate,nper,pv) {
        var pvif, pmt;
    
        pvif = Math.pow( 1 + rate, nper);
        pmt = rate / (pvif - 1) * -(pv * pvif);   
    
        return pmt;
    };

    // amortization schedule
    function computeSchedule(loan_amount, interest_rate, payments_per_year, years, payment) {
        var schedule = [];
        var remaining = loan_amount;
        var number_of_payments = payments_per_year * years;
        var previousInterestValue = 0;
        var previousPrincipleValue = 0;
        for (var i=0; i<=number_of_payments; i++) {
            var interest = remaining * (interest_rate/100/payments_per_year);
            var intNext = interest + previousInterestValue;
            var principle = (payment-interest);
            var prinNext = principle + previousPrincipleValue;
            var row = [i, prinNext > 0 ? Math.round(prinNext) : 0, intNext > 0 ? Math.round(intNext) : 0, remaining > 0 ? Math.round(remaining) : 0 ];
            schedule.push(row);
            remaining -= principle
            previousInterestValue = intNext;
            previousPrincipleValue = prinNext;
        }
        return schedule;
    }

    function chart_mortgagePaymentSchedule() {
        var interest = [], principal_remaining = [], principal = [],
            interestData = [], principalRemainingData = [], principalData = [];

        $scope.payment = pmt($scope.interest_rate_per_month, $scope.payments_per_year * $scope.years, -$scope.loan_amount).toFixed(2);
        $scope.schedule = computeSchedule($scope.loan_amount, $scope.interest_rate, $scope.payments_per_year, $scope.years, $scope.payment);

        $scope.schedule.forEach(function(element) {
            principalData.push(element[1]);
            interestData.push(element[2]);
            principalRemainingData.push(element[3]);
        }, this);

        // console.log($scope.schedule);
        
        // console.log(interestData.length);
        for (var i = 0; i < interestData.length; i++) {
            interest.push({ x: i, y: interestData[i] });
            principal_remaining.push({ x: i, y: principalRemainingData[i] });
            // principal.push({ x: i, y: principalData[i] });
        }

        //Line chart data should be sent as an array of series objects.
        return [
            {
                values: interest, //values - represents the array of {x,y} data points
                key: 'Interest', //key  - the name of the series.
                color: '#2a80b9', //color - optional: choose your own line color.
                strokeWidth: 2
            },
            {
                values: principal_remaining,
                key: 'Principal over the 30-year amortization of the loan',
                color: '#2ca02c',
                strokeWidth: 2
            },
            // {
            //     values: principal,
            //     key: 'Principal',
            //     color: '#2ca02c',
            //     strokeWidth: 2
            // }
        ];
    };
    
    function chart_rentVsBuy() {
        var totalRent = [], totalBuy = [],
            totalRentData = [], totalBuyData = []

        $scope.payment = pmt($scope.interest_rate_per_month, $scope.payments_per_year * $scope.years, -$scope.loan_amount).toFixed(2);
        $scope.schedule = computeSchedule($scope.loan_amount, $scope.interest_rate, $scope.payments_per_year, $scope.years, $scope.payment);

        $scope.schedule.forEach(function(element) {
            principalData.push(element[1]);
            interestData.push(element[2]);
            principalRemainingData.push(element[3]);
        }, this);

        console.log($scope.schedule);
        
        // console.log(interestData.length);
        for (var i = 0; i < interestData.length; i++) {
            interest.push({ x: i, y: interestData[i] });
            principal_remaining.push({ x: i, y: principalRemainingData[i] });
        }

        //Line chart data should be sent as an array of series objects.
        return [
            {
                values: interest, //values - represents the array of {x,y} data points
                key: 'Interest', //key  - the name of the series.
                color: '#2a80b9', //color - optional: choose your own line color.
                strokeWidth: 2
            },
            {
                values: principal_remaining,
                key: 'Principal over the 30-year amortization of the loan',
                color: '#2ca02c',
                strokeWidth: 2
            }
        ];
    };
});

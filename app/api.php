<?php

$url = 'https://www.mortgageloan.com/calculator/load-single-calculator';

switch($_POST['calculator']) {
        
    case 'MortgagePaymentCalculator':
        $fields = array(
            'calculator' => 'MortgagePaymentCalculator',
            'template' => 'SingleMortgagePaymentCalculator',
            'calculatorData[calculator_widget][Amount]' => $_POST['calculatorData']['calculator_widget']['Amount'],
            'calculatorData[calculator_widget][Interest]' => $_POST['calculatorData']['calculator_widget']['Interest'],
            'calculatorData[calculator_widget][Length]' => $_POST['calculatorData']['calculator_widget']['Length'],
            'calculatorData[calculator_widget][HomeValue]' => $_POST['calculatorData']['calculator_widget']['HomeValue'],
            'calculatorData[calculator_widget][PropertyTaxes]' => $_POST['calculatorData']['calculator_widget']['PropertyTaxes'],
            'calculatorData[calculator_widget][PropertyTaxesSel]' => $_POST['calculatorData']['calculator_widget']['PropertyTaxesSel'],
            'calculatorData[calculator_widget][Insurance]' => $_POST['calculatorData']['calculator_widget']['Insurance'],
            'calculatorData[calculator_widget][InsuranceSel]' => $_POST['calculatorData']['calculator_widget']['InsuranceSel'],
            'calculatorData[calculator_widget][PMI]' => $_POST['calculatorData']['calculator_widget']['PMI'],
            'calculatorData[calculator_widget][PMISel]' => $_POST['calculatorData']['calculator_widget']['PMISel'],
        );
        break;
        
    case 'BuyVsRentCalculator':
        $fields = array(
            'calculator' => 'BuyVsRentCalculator',
            'template' => 'SingleBuyVsRentCalculator',
            'calculatorData' => array(
                'calculator_widget' => array(
                    'PeriodRent' => $_POST['calculatorData']['calculator_widget']['PeriodRent'],
                    'AnnualRentIncrease' => $_POST['calculatorData']['calculator_widget']['AnnualRentIncrease'],
                    'HomeValue' => $_POST['calculatorData']['calculator_widget']['HomeValue'],
                    'AnnualMaintanance' => $_POST['calculatorData']['calculator_widget']['AnnualMaintanance'],
                    'AnnualAppreciation' => $_POST['calculatorData']['calculator_widget']['AnnualAppreciation'],
                    'YearsBeforeSell' => $_POST['calculatorData']['calculator_widget']['YearsBeforeSell'],
                    'SellingCost' => $_POST['calculatorData']['calculator_widget']['SellingCost'],
                    'Amount' => $_POST['calculatorData']['calculator_widget']['Amount'],
                    'Interest' => $_POST['calculatorData']['calculator_widget']['Interest'],
                    'Length' => $_POST['calculatorData']['calculator_widget']['Length'],
                    'Points' => $_POST['calculatorData']['calculator_widget']['Points'],
                    'Taxes' => $_POST['calculatorData']['calculator_widget']['Taxes'],
                    'PropertyTaxes' => $_POST['calculatorData']['calculator_widget']['PropertyTaxes'],
                    'PropertyTaxesSel' => $_POST['calculatorData']['calculator_widget']['PropertyTaxesSel'],
                    'Insurance' => $_POST['calculatorData']['calculator_widget']['Insurance'],
                    'InsuranceSel' => $_POST['calculatorData']['calculator_widget']['InsuranceSel'],
                    'PMI' => $_POST['calculatorData']['calculator_widget']['PMI'],
                    'PMISel' => $_POST['calculatorData']['calculator_widget']['PMISel'],
                )
            )
        );
        break;
        
    default:
        break;
}

//url-ify the data for the POST
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch);
$err = curl_error($ch);

//close connection
curl_close($ch);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $result;
}


// var_dump($_POST);
